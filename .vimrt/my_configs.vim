set nocompatible
filetype plugin on
filetype indent on
syntax on
set hlsearch
set background=dark
set encoding=utf8
set nobackup
set nowb
set noswapfile
set expandtab
set relativenumber

let mapleader = ","

" vim-plug directory
call plug#begin('~/.vimrt/plugins')

Plug 'tpope/vim-fugitive'
Plug 'dracula/vim'
Plug 'francoiscabrol/ranger.vim'
Plug 'tpope/vim-commentary'
Plug 'vimwiki/vimwiki'
" Plug 'nvie/vim-flake8'
" Plug 'vim-airline/vim-airline'
" Plug 'itchyny/lightline.vim'
" Plug 'valloric/youcompleteme'
Plug 'rust-lang/rust.vim'
call plug#end()

colorscheme dracula

let g:vimwiki_list = [{'path': '~/Nextcloud/wiki', 'syntax': 'markdown', 'ext': '.md'}]

" try
    " set spell spelllang=de_ch
" endtry

" set lines=100 columns=80

" let g:ycm_python_interpreter_path = ''
" let g:ycm_python_sys_path = []
" let g:ycm_extra_conf_vim_data = [
"   \  'g:ycm_python_interpreter_path',
"   \  'g:ycm_python_sys_path'
"   \]
" let g:ycm_global_ycm_extra_conf = '~/global_extra_conf.py'

" Smart way to move between windows
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => GUI related
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Set font according to system
if has("mac") || has("macunix")
    set gfn=IBM\ Plex\ Mono:h14,Hack:h14,Source\ Code\ Pro:h15,Menlo:h15
elseif has("win16") || has("win32")
    set gfn=IBM\ Plex\ Mono:h14,Source\ Code\ Pro:h12,Bitstream\ Vera\ Sans\ Mono:h11
elseif has("gui_gtk2")
    set gfn=IBM\ Plex\ Mono\ 14,:Hack\ 14,Source\ Code\ Pro\ 12,Bitstream\ Vera\ Sans\ Mono\ 11
elseif has("linux")
    set gfn=IBM\ Plex\ Mono\ 14,:Hack\ 14,Source\ Code\ Pro\ 12,Bitstream\ Vera\ Sans\ Mono\ 11
elseif has("unix")
    set gfn=Monospace\ 11
endif

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => General abbreviations
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
iab xdate <C-r>=strftime("%y/%m/%d")<cr>
iab xtime <C-r>=strftime("%H/:%M")<cr>

""""""""""""""""
" Splash screen
""""""""""""""""

" fun! Start()

  "Create a new unnamed buffer to display our splash screen inside of.
  " enew

  " Set some options for this buffer to make sure that does not act like a
  " normal winodw.
  " setlocal
  "   \ bufhidden=wipe
  "   \ buftype=nofile
  "   \ nobuflisted
  "   \ nocursorcolumn
  "   \ nocursorline
  "   \ nolist
  "   \ nonumber
  "   \ noswapfile
  "   \ norelativenumber

  " Our message goes here. Mine is simple.
  " call append('$', "hello")
      " exec ":r !fortune"

  " When we are done writing out message set the buffer to readonly.
  " setlocal
  "   \ nomodifiable
    " \ nomodified

  " Just like with the default start page, when we switch to insert mode
  " a new buffer should be opened which we can then later save.
  " nnoremap <buffer><silent> e :enew<CR>
  " nnoremap <buffer><silent> i :enew <bar> startinsert<CR>
  " nnoremap <buffer><silent> o :enew <bar> startinsert<CR>

" endfun

" http://learnvimscriptthehardway.stevelosh.com/chapters/12.html
" if argc() == 0
  " autocmd VimEnter * call Start()
" endif
